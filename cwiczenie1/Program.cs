﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Napisać program konsolowy z wykorzystaniem pętli while, w którym będziemy
            // emitować dźwięk(Console.Beep()) przez 700 milisekund o częstotliwości od 500 
            // do 15000 Hz, co 500 Hz.
            // Przy każdej rundzie pętli wyświetlić w konsoli jakiej częstotliwości dźwięk
            // będzie za chwilę emitowany.
            int czestotliwosc = 500;
            const int czasTrwaniaDzwieku = 700;
            while(czestotliwosc <= 15000)
            {
                Console.WriteLine("Emitowana częstotliwość: {0}", czestotliwosc);
                Console.Beep(czestotliwosc, czasTrwaniaDzwieku);
                czestotliwosc += 500;
            }
        }
    }
}
